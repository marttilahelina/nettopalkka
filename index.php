<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="nettopalkka.css">
    <title>Nettopalkka</title>
</head>
<body>
    <h2>Nettopalkan laskenta</h2>   
    <div class="row"> 
          <div class="col vas">
             
    <form action= "<?php print($_SERVER['PHP_SELF']); ?>" method="POST" >
            <p><label for="brutto" name="brutto">Bruttopalkka:</label>
            <input name="brutto" id="brutto" type="number" step="0.01" min=0> €</p>
            <p><label name="ennpid">Ennakonpidätys:</label>
            <input name="ennpid" type="number" step="0.1" min=0 max=60> %</p>
            <p><label for="tyel" name="tyel">Työeläkemaksu:</label>
            <select id="tyel" name="tyel">
               <option>0</option>
               <option>7.15</option>
               <option>8.65</option>
            </select> %<p>
            <p><label for="tvm" name="tvm">Työntekijän työttömyysvakuutusmaksu:</label>
            <select id="tvm" name="tvm">
               <option>0</option>
               <option>1.25</option>
            </select> %</p>
            <button>Laske nettopalkka</button>
            </form>
            <p>Kuinka työntekijän ikä vaikuttaa työeläke- ja työttömyysvakuutusmaksun prosentteihin?<br>
                <a href="https://www.vero.fi/yritykset-ja-yhteisot/tietoa-yritysverotuksesta/yritys_tyonantajana/sosiaalivakuutusmaksut/" 
                target="_blank">Verohallinnon ohje sosiaalivakuutusmaksuista</a>
             </p> 
         </div>
      </div>

   <div class="col oik">
      <div>
   <?php
      $brutto = 0;
      $ennpid = 0;
      $tyel = 0;
      $tvm = 0;
      $ennpidos = 0;
      $tyelos = 0;
      $tvmos = 0;
      $netto = 0;

      if($_SERVER['REQUEST_METHOD']=='POST'){
        $brutto = filter_input(INPUT_POST, 'brutto', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $ennpid = filter_input(INPUT_POST, 'ennpid', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tyel = filter_input(INPUT_POST, 'tyel', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tvm = filter_input(INPUT_POST, 'tvm', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        
        $ennpidos = $brutto * ($ennpid/100);
        $tyelos = $brutto * ($tyel/100);
        $tvmos = $brutto * ($tvm/100);
        $netto = $brutto - $ennpidos - $tyelos - $tvmos;

        printf("<p>Bruttopalkka on %.2f euroa.</p>", $brutto);
        printf("<p>Ennakonpidätys palkasta on %.2f euroa.</p>", $ennpidos);
        printf("<p>Työeläkemaksun osuus on %.2f euroa.</p>", $tyelos);
        printf("<p>Työttömyysvakuutusmaksu on %.2f euroa.</p>", $tvmos);
        printf("<p>Nettopalkka on %.2f euroa.</p>", $netto);
      }
    ?>
    </div>
   </div>
</body>
</html>